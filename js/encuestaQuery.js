$(document).ready(function(){
    $.handlerErrors()
})

$.handlerErrors = function() {
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
      }, "No se permiten Numeros");

    $('#formulario').validate({
        rules: {
            nombre: {
                required: true,
                lettersonly: true,
                minlength: 4,
            },
            apellido: {
                required: true,
                lettersonly: true,
                minlength: 4,
            },
            nacimiento: {
                required: true,
                date: true
            },
        },
        messages: {
            nombre: {
                required: 'el campo es requerido',
                minlength: 'minimo 4 letras'
            },
            apellido: {
                required: 'el campo es requerido',
                minlength: 'minimo 4 letras'
            },
            nacimiento: {
                required: 'el campo es requerido',
                date: 'tiene que ingresar una fecha valida'
            },

        },    
        submitHandler: function(form, event) { 
            event.preventDefault();
            form.submit()
            //submit via ajax
          } 
        }
    )
}

function cancelar() {
    return confirm('Seguro que desea cancelar')
}