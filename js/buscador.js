$(document).ready(function() {
    $.getJSON( "js/keys.json", function( data ) {
        sessionStorage.setItem('nasa_key', data.NASA_API)
        $.roversAElegir()
        $.historial()
        $('input[name=fecha]').on('change', function() {
            $.fecha()
        })
        $('input[name=rover]').on('change', function() {
            $.selects()
        })

        $('#buscar').on('click', function () {
            $.getData()
            $.historial()
        })
        $('#historial-selected').on('change', function () {
            $.showData()
            $.historial()
        })

    })
})

$.roversAElegir = function() {
    var roversAElegir = JSON.parse(localStorage.getItem('rovers'))
    var output = ''    
    $.each(roversAElegir, function(key, value) {
        output += ' ' + value.name + ' '
        output += '<input type="radio" name="rover" value="'+ value.name +'">'
    })
    $("#robots").html(output)
}
$.showData = function() {
    var historial = $('#historial-selected').val() ? $('#historial-selected').val() : null
    var data = JSON.parse(JSON.parse(localStorage.getItem('historial'))[historial])
    console.log('historial', data, historial)
    var output = ''
    $.each(data.data.photos, function(key, value) {
        output += '<article class="item" id="'+value.id+'">'
        output += '<img class="foto" src="'+ value.img_src +'" alt="foto_nasa">'
        output += '<p class="descripcion"> Fecha radio solar de marte: '+ value.sol +"</p>"
        output += '<p class="descripcion"> Fecha terrestre de la foto: '+ value.earth_date +"</p>"
        output += '<p class="descripcion"> Imagen tomada por la camara: '+ value.camera.full_name +"</p>"
        output += '<button class="boton-navigator">'
        output += '<a class="boton-nav-compartir" href="compartir.html">Compartir con un Amigo</a>' 
        output += '</button>'
        output += '</article>';
    })
    $("#contenedor").html(output)
}
$.historial = function() {
    var historial = JSON.parse(localStorage.getItem('historial'))
    if (historial == null) {
        $("#historial-select").hide()
        $("#historial").hide()
        return 0
    }
    var output = ''    
    output += '<select id="historial-selected" class="select">'
    output += '<option value=""> Historial'
    $.each(historial, function(key, value) {
        output += '<option value="'+ key + '">' + JSON.parse(value).name          
    })

    $("#historial-select").html(output)
    $("#historial-select").show()
    $("#historial").show()
}
$.fecha = function() {
    var fecha = $('input[name=fecha]:checked').val()
    if (fecha == 'terrestre') {
        $('#tierra').show()
        $('#sol').hide()
    }
    if (fecha == 'marte') {
        $('#sol').show()
        $('#tierra').hide()
    }
}

$.selects = function() {
    var rover = $('input[name=rover]:checked').val()
    var roversAElegir = JSON.parse(localStorage.getItem('rovers'))
    var output = '<div>Cámara a elegir:</div>'
    output += '<select id="camara-select" class="select">'
    $.each(roversAElegir, function(key, value) {
        if (value.name == rover) {
            $.each (value.cameras, function(key,camara){
                output += '<option value="'+ camara.name + '">' + camara.full_name          
            }) 
        }
        
    }) 
    output += '</select>'

    $("#camara-elegir").html(output)
}
$.appendToStorage = function(name, data) {
    var old = JSON.parse(localStorage.getItem(name))
    if(old === null) old = []
    old.push(data)
    localStorage.setItem(name, JSON.stringify(old) )
}

$.getData = function() {
    var myUrl = 'https://api.nasa.gov/mars-photos/api/v1/rovers/'
    var rover = $('input[name=rover]:checked').val()
    var selectorFecha = $('input[name=fecha]:checked').val()
    var camara = $('#camara-select').val() ? $('#camara-select').val() : null
    var fecha = ''
    var page = $('#page').val() ? $('#page').val() : 1
    myUrl += rover + '/photos?api_key=' + sessionStorage.getItem('nasa_key')
    var query = {
    camera: camara,
    page: page
    }
    if (selectorFecha == 'terrestre') {
        fecha = $('#fechaTerrestre').val() ? $('#fechaTerrestre').val() : null
        query.earth_date = fecha
    }    
    if (selectorFecha == 'marte') {
        fecha = $('#maxSol').val() ? $('#maxSol').val() : null
        query.sol = fecha
    }

    try {
        $.ajax({
            type: 'GET',
            url: myUrl,
            contentType: 'application/json',
            data: query,
            success: function(data,status) {
                var output = ''
                // recorremos los valores de cada usuario
                if (!data.photos.length) {
                    output += '<article class="item">'
                    output += '<p class="text-error"> No se encontraron resultados para la busqueda</p>'
                    output += '</article>';
                } else {
                    var index = localStorage.getItem('historial') ? JSON.parse(localStorage.getItem('historial')).length + 1  : 1
                    console.log (index)
                    var historial = {
                        name: 'Busqueda Nº' + index,
                        data: data,
                    }
                    $.appendToStorage('historial', JSON.stringify(historial) ) 

                }
                if (data.photos.length) {
                    $.each(data.photos, function(key, value) {
                        output += '<article class="item" id="'+value.id+'">'
                        output += '<img class="foto" src="'+ value.img_src +'" alt="foto_nasa">'
                        output += '<p class="descripcion"> Fecha radio solar de marte: '+ value.sol +"</p>"
                        output += '<p class="descripcion"> Fecha terrestre de la foto: '+ value.earth_date +"</p>"
                        output += '<p class="descripcion"> Imagen tomada por la camara: '+ value.camera.full_name +"</p>"
                        output += '<button class="boton-navigator">'
                        output += '<a class="boton-nav-compartir" href="compartir.html">Compartir con un Amigo</a>' 
                        output += '</button>'
                        output += '</article>';
                    })
                }
                $("#contenedor").html(output)
            },
            error: function(status,error) {
                alert ('error')
            },
            dataType: 'json'
        })
    } catch(e) {
        alert('paso algo feo')
    }
;
}
