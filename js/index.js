$(document).ready(function() {
    $.getJSON( "js/keys.json", function( data ) {
        sessionStorage.setItem('nasa_key', data.NASA_API)
        $.getData()
    });
})

$.getData = function() {
    var myUrl = 'https://api.nasa.gov/mars-photos/api/v1/rovers?api_key='
    myUrl += sessionStorage.getItem('nasa_key')
    try {
        $.ajax({
            type: 'GET',
            url: myUrl,
            contentType: 'application/json',
            success: function(data,status) {
                localStorage.setItem('rovers', JSON.stringify(data.rovers))
                console.log(JSON.parse(localStorage.rovers))
                var output = ''    
                // recorremos los valores de cada usuario
                $.each(data.rovers, function(key, value) {
                    var camarasVigentes = 'Sus camaras vigentes son las siguientes:'
                    var lastCamera = value.cameras.length - 1
                    $.each(value.cameras, function(index,camara) {
                        if (lastCamera == index) {
                            camarasVigentes += ' ' + camara.full_name + '.'
                        } else {
                            camarasVigentes += ' ' + camara.full_name + ','
                        }
                    })
                    output += '<div class="item" id="'+value.id+'">'
                    output += '<article>'
                    output += '<h1 class="nombre"> Rover: '+ value.name +"</h1>"
                    output += '<h2 class="status"> Estado: '+ value.status +"</h2>"
                    output += '<p class="fechas"> Fecha de salida de la tierra: <strong>'+ value.launch_date +"</strong></p>"
                    output += '<p class="fechas"> Aterrizaje en Marte: <strong>'+ value.landing_date +"</strong></p>"
                    output += '<p class="fechas"> Fecha de la ultima Foto: <strong>'+ value.max_date +"</strong></p>"
                    output += '<p class="fechas"> Fecha del radio maximo solar de marte: <strong>'+ value.max_sol +"</strong></p>"
                    output += '<p class="fechas"> Cantidad de fotos tomadas: <strong>'+ value.total_photos +"</strong></p>"
                    output += '<p class="camaras"> <strong>'+ camarasVigentes +"</strong></p>"
                    output += '</article>'
                    output += '</div>'
                })
                $("#rovers").html(output)
            },
            error: function(status,error) {
                alert ('error')
            },
            dataType: 'json'
        })
    } catch(e) {
        alert('paso algo feo')
    }

;
}
