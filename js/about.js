$(document).ready(function() {
    $.getJSON( "js/keys.json", function( data ) {
        sessionStorage.setItem('google_key', data.GOOGLE_API)
    });
    var script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + sessionStorage.getItem('google_key') + '&callback=initMap' 
    script.async = false;
    document.head.appendChild(script);
})

function initMap() {
  var myLatLng = {lat: -34.922883, lng: -57.956317};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: myLatLng,
    styles: [
      {position: 'relative'}
    ],
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Aqui trabajo!'
  });
}